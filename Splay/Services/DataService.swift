//
//  DataService.swift
//  Splay
//
//  Created by Faizan on 11/21/17.
//  Copyright © 2017 Faizan. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class DataService
{
    static let instance = DataService()
    var filmGenre: String = ""
    var upcomingGenre: String = ""
    var nowplaying = [Movie]()
    var popular = [Movie]()
    var upcoming = [Movie]()
    var similar = [Movie]()
    var search = [Movie]()
    var topRated = [Movie]()
    var cast = [Cast]()
    
    func NowPlaying(completion: @escaping CompletionHandler)
    {
        Alamofire.request(NOW_PLAYING_URL).responseJSON { (response) in
            
            if response.result.error == nil
            {
                guard let data = response.data else {return}
                let json = try! JSON(data: data)
                if let results = json["results"].array
                {
                    for result in results
                    {
                        let title = result["title"].stringValue
                        let id = result["id"].intValue
                        let vote = result["vote_average"].floatValue
                        let poster = result["poster_path"].stringValue
                        if let genreIds = result["genre_ids"].array
                        {
                            for genreId in genreIds
                            {
                                
                                if let path = Bundle.main.path(forResource: "Genre", ofType: "json")
                                {
                                    let data1 = try! Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                                    let jsonResult = try! JSON(data: data1)
                                    if let genres = jsonResult["genres"].array
                                    {
                                        for genre in genres
                                        {
                                            let genre_id  = genre["id"].intValue
                                            if genre_id == genreId.intValue
                                            {
                                                let name = genre["name"].stringValue
                                                self.filmGenre.append("\(name), ")
                                            }
                                        }
                                    }
                                }
                            
                                
                            }
                        }
                        let movie = Movie(id: id, vote: vote, title: title, overview: "", release_date: "", poster: poster, backdrop: "", genre: self.filmGenre, tagline: "")
                        self.nowplaying.append(movie)
                        self.filmGenre = ""

                    }
                    completion(true, "")
                }
                else
                {
                    let error = json["status_message"].stringValue
                    completion(false, error)
                }
            }
            else
            {
                completion(false, String(describing: response.result.error!.localizedDescription))
            }
        }
    }
    
    
    func popularMovies(completion: @escaping CompletionHandler)
    {
        Alamofire.request(POPULAR_URL).responseJSON { (response) in
            
            if response.result.error == nil
            {
                guard let data = response.data else {return}
                let json = try! JSON(data: data)
                if let results = json["results"].array
                {
                    for result in results
                    {
                        let title = result["title"].stringValue
                        let id = result["id"].intValue
                        let vote = result["vote_average"].floatValue
                        let poster = result["poster_path"].stringValue
                        let overview = result["overview"].stringValue
                        if let genreIds = result["genre_ids"].array
                        {
                            for genreId in genreIds
                            {
                                
                                if let path = Bundle.main.path(forResource: "Genre", ofType: "json")
                                {
                                    let data1 = try! Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                                    let jsonResult = try! JSON(data: data1)
                                    if let genres = jsonResult["genres"].array
                                    {
                                        for genre in genres
                                        {
                                            let genre_id  = genre["id"].intValue
                                            if genre_id == genreId.intValue
                                            {
                                                let name = genre["name"].stringValue
                                                self.filmGenre.append("\(name), ")
                                            }
                                        }
                                    }
                                }
                                
                                
                            }
                        }
                        let movie = Movie(id: id, vote: vote, title: title, overview: overview, release_date: "", poster: poster, backdrop: "", genre: self.filmGenre, tagline: "")
                        self.popular.append(movie)
                        self.filmGenre = ""
                        
                    }
                    completion(true, "")
                }
                else
                {
                    let error = json["status_message"].stringValue
                    completion(false, error)
                }
            }
            else
            {
                completion(false, String(describing: response.result.error!.localizedDescription))
            }
        }
    }
    
    
    func upcomingMovies(completion: @escaping CompletionHandler)
    {
        Alamofire.request(UPCOMING_URL).responseJSON { (response) in
            
            if response.result.error == nil
            {
                guard let data = response.data else {return}
                let json = try! JSON(data: data)
                if let results = json["results"].array
                {
                    for result in results
                    {
                        let title = result["title"].stringValue
                        let id = result["id"].intValue
                        let poster = result["poster_path"].stringValue
                        let overview = result["overview"].stringValue
                        let vote = result["vote_average"].floatValue
                        if let genreIds = result["genre_ids"].array
                        {
                            for genreId in genreIds
                            {
                                
                                if let path = Bundle.main.path(forResource: "Genre", ofType: "json")
                                {
                                    let data1 = try! Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                                    let jsonResult = try! JSON(data: data1)
                                    if let genres = jsonResult["genres"].array
                                    {
                                        for genre in genres
                                        {
                                            let genre_id  = genre["id"].intValue
                                            if genre_id == genreId.intValue
                                            {
                                                let name = genre["name"].stringValue
                                                self.upcomingGenre.append("\(name), ")
                                            }
                                        }
                                    }
                                }
                                
                                
                            }
                        }
                        let movie = Movie(id: id, vote: vote, title: title, overview: overview, release_date: "", poster: poster, backdrop: "", genre: self.upcomingGenre, tagline: "")
                        self.upcoming.append(movie)
                        self.upcomingGenre = ""
                    }
                    completion(true, "")
                }
                else
                {
                    let error = json["status_message"].stringValue
                    completion(false, error)
                }
            }
            else
            {
                completion(false, String(describing: response.result.error!.localizedDescription))
            }
        }
    }
    
  
    
    func RecommendedMovies(movieId: Int, completion: @escaping CompletionHandler)
    {
        Alamofire.request("\(MOVIE_URL)\(movieId)/recommendations?api_key=\(API_KEY)").responseJSON { (response) in
            
            if response.result.error == nil
            {
                guard let data = response.data else {return}
                let json = try! JSON(data: data)
                self.similar = []
                if let results = json["results"].array
                {
                    for result in results
                    {
                        let title = result["title"].stringValue
                        let id = result["id"].intValue
                        let vote = result["vote_average"].floatValue
                        let poster = result["poster_path"].stringValue
                        if let genreIds = result["genre_ids"].array
                        {
                            for genreId in genreIds
                            {
                                
                                if let path = Bundle.main.path(forResource: "Genre", ofType: "json")
                                {
                                    let data1 = try! Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                                    let jsonResult = try! JSON(data: data1)
                                    if let genres = jsonResult["genres"].array
                                    {
                                        for genre in genres
                                        {
                                            let genre_id  = genre["id"].intValue
                                            if genre_id == genreId.intValue
                                            {
                                                let name = genre["name"].stringValue
                                                self.filmGenre.append("\(name), ")
                                            }
                                        }
                                    }
                                }
                                
                                
                            }
                        }
                        let movie = Movie(id: id, vote: vote, title: title, overview: "", release_date: "", poster: poster, backdrop: "", genre: self.filmGenre, tagline: "")
                        self.similar.append(movie)
                        self.filmGenre = ""
                        
                    }
                    completion(true, "")
                }
                else
                {
                    let error = json["status_message"].stringValue
                    completion(false, error)
                }
            }
            else
            {
                completion(false, String(describing: response.result.error!.localizedDescription))
            }
        }
    }
    
    
    func searchMovies(query: String, completion: @escaping CompletionHandler)
    {

        Alamofire.request("\(SEARCH_URL)&query=\(query)").responseJSON { (response) in
            
         
            if response.result.error == nil
            {
                guard let data = response.data else {return}
                let json = try! JSON(data: data)
                self.search = []
                if let results = json["results"].array
                {
                    for result in results
                    {
                        let title = result["title"].stringValue
                        let id = result["id"].intValue
                        let poster = result["poster_path"].stringValue
                        let overview = result["overview"].stringValue
                        let vote = result["vote_average"].floatValue
                        let release_date = result["release_date"].stringValue
                        let movie = Movie(id: id, vote: vote, title: title, overview: overview, release_date: release_date, poster: poster, backdrop: "", genre: "", tagline: "")
                        
                        self.search.append(movie)
                    }
                    completion(true, "")
                }
                else
                {
                    let error = json["status_message"].stringValue
                    completion(false, error)
                }
            }
            else
            {
                completion(false, String(describing: response.result.error!.localizedDescription))
            }
        }
    }
    
    
    func castMovies(movieID: Int, completion: @escaping CompletionHandler)
    {
        Alamofire.request("\(MOVIE_URL)\(movieID)/credits?api_key=\(API_KEY)").responseJSON { (response) in
            if response.result.error == nil
            {
                guard let data = response.data else {return}
                let json = try! JSON(data: data)
                self.cast = []
                if let results = json["cast"].array
                {
                    for result in results
                    {
                        let title = result["name"].stringValue
                        let profile_path = result["profile_path"].stringValue
                        
                        let MovieCast = Cast(cast: title, profile: profile_path)
                        self.cast.append(MovieCast)
                    }
                    completion(true, "")
                }
                else
                {
                    let error = json["status_message"].stringValue
                    completion(false, error)
                }
            }
            else
            {
                completion(false, String(describing: response.result.error!.localizedDescription))
            }
        }
    }
    
    
    
    
    func getTopRatedMovies(completion: @escaping CompletionHandler)
    {
        Alamofire.request(TOP_RATED_URL).responseJSON { (response) in
            if response.result.error == nil
            {
                guard let data = response.data else {return}
                let json = try! JSON(data: data)
                self.topRated = []
                if let results = json["results"].array
                {
                    for result in results
                    {
                        let title = result["title"].stringValue
                        let id = result["id"].intValue
                        let poster = result["poster_path"].stringValue
                        let overview = result["overview"].stringValue
                        let vote = result["vote_average"].floatValue
                        let release_date = result["release_date"].stringValue
                        let movie = Movie(id: id, vote: vote, title: title, overview: overview, release_date: release_date, poster: poster, backdrop: "", genre: "", tagline: "")
                        self.topRated.append(movie)
                    }
                    completion(true, "")
                }
                else
                {
                    let error = json["status_message"].stringValue
                    completion(false, error)
                }
            }
            else
            {
                completion(false, String(describing: response.result.error!.localizedDescription))
            }
        }
    }
    
    
    
    
    
    
}

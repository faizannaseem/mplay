//
//  Splay.swift
//  Splay
//
//  Created by Faizan on 11/21/17.
//  Copyright © 2017 Faizan. All rights reserved.
//

import Foundation
import RealmSwift

class Movie
{
    private var _id: Int!
    private var _vote: Float!
    private var _title: String!
    private var _overview: String!
    private var _release_date: String!
    private var _poster: String!
    private var _backdrop: String!
    private var _genre: String!
    private var _tagline: String!
    
    var id : Int
    {
        return _id
    }
    
    var vote: Float
    {
        return _vote
    }
    
    var title: String
    {
        return _title
    }
    
    var overview: String
    {
        return _overview
    }
    
    var release_date: String
    {
        return _release_date
    }
    
    var poster: String
    {
        return _poster
    }
    
    var backdrop: String
    {
        return _backdrop
    }
    
    var genre: String
    {
        return _genre
    }
    
    var tagline: String
    {
        return _tagline
    }
    
    
    init(id : Int, vote: Float, title: String, overview: String, release_date: String,
poster: String, backdrop: String, genre: String, tagline: String)
    {
        self._backdrop = backdrop
        self._genre = genre
        self._id = id
        self._overview = overview
        self._poster = poster
        self._release_date = release_date
        self._tagline = tagline
        self._title = title
        self._vote = vote
    }

    
    
}


class Cast
{
    private var _profile_path : String!
    private var _castName: String!
    
    var profile_path: String
    {
        return _profile_path
    }
    
    var cast_Name: String
    {
        return _castName
    }
    
    init(cast: String, profile: String) {
        self._castName = cast
        self._profile_path = profile
    }
    
}


class MovieDB: Object
{
    
    @objc dynamic var date: Date!
    @objc dynamic var id: Int = 0
    @objc dynamic var vote: Float = 0
    @objc dynamic var genre: String = ""
    @objc dynamic var title: String = ""
    @objc dynamic var taskId = UUID().uuidString
    @objc dynamic var poster: Data!
    
    override class func primaryKey() -> String?
    {
        return "taskId"
    }
    
}












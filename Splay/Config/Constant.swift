//
//  Constant.swift
//  Splay
//
//  Created by Faizan on 12/22/17.
//  Copyright © 2017 Faizan. All rights reserved.
//

import Foundation


typealias CompletionHandler = (_ Success : Bool, _ Error : String?) -> ()

let API_KEY = "d5b58646f7c4f89fd501495a5cec52ae"
let ADMOB_APP_ID = "ca-app-pub-5765623355505954~4206287569"
let ADMOB_APP_UNIT = "ca-app-pub-5765623355505954/1064639975"
let MOVIE_URL = "https://api.themoviedb.org/3/movie/"
let IMAGE_URL = "https://image.tmdb.org/t/p/w500"
let NOW_PLAYING_URL = "\(MOVIE_URL)now_playing?api_key=\(API_KEY)"
let POPULAR_URL = "\(MOVIE_URL)popular?api_key=\(API_KEY)"
let UPCOMING_URL = "\(MOVIE_URL)upcoming?api_key=\(API_KEY)"
let TOP_RATED_URL = "\(MOVIE_URL)top_rated?api_key=\(API_KEY)"
let SEARCH_URL = "https://api.themoviedb.org/3/search/movie?api_key=\(API_KEY)&language=en-US&page=1&include_adult=false"

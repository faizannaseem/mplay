//
//  WishlistCell.swift
//  Splay
//
//  Created by Faizan on 12/29/17.
//  Copyright © 2017 Faizan. All rights reserved.
//

import UIKit

class WishlistCell: UITableViewCell {

    @IBOutlet weak var voteLbl: UILabel!
    @IBOutlet weak var movieGenre: UILabel!
    @IBOutlet weak var movieName: UILabel!
    @IBOutlet weak var movieImg: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        movieImg.layer.cornerRadius = 3.0
        
    }
    
    
    func configureCell(movie: MovieDB)
    {
        voteLbl.text = "\(movie.vote)"
        movieGenre.text = movie.genre
        movieName.text = movie.title
        movieImg.image = UIImage(data: movie.poster)
    }

}

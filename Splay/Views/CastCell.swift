//
//  CastCell.swift
//  Splay
//
//  Created by Faizan on 12/29/17.
//  Copyright © 2017 Faizan. All rights reserved.
//

import UIKit

class CastCell: UICollectionViewCell {
    
    @IBOutlet weak var castName: UILabel!
    @IBOutlet weak var castImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        castImg.layer.cornerRadius = castImg.bounds.width / 2
    }
    
    func configureCell(cast: Cast)
    {
    
        castName.text = cast.cast_Name
        castImg.sd_setImage(with: URL(string: "\(IMAGE_URL)\(cast.profile_path)"), placeholderImage: UIImage(named: "user.png"), options: [.continueInBackground, .progressiveDownload], completed: nil)
        
    }
    
}

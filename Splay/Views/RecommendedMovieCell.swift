//
//  RecommendedMovieCell.swift
//  Splay
//
//  Created by Faizan on 01/03/2018.
//  Copyright © 2018 Faizan. All rights reserved.
//

import UIKit

class RecommendedMovieCell: UICollectionViewCell {
    
    @IBOutlet weak var voteLbl: UILabel!
    @IBOutlet weak var movieGenre: UILabel!
    @IBOutlet weak var movieName: UILabel!
    @IBOutlet weak var movieImg: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        movieImg.layer.cornerRadius = 3.0
        
    }
    
    
    func configureCell(movie: Movie)
    {
        voteLbl.text = "\(movie.vote)"
        movieGenre.text = movie.genre
        movieName.text = movie.title
        movieImg.sd_setImage(with: URL(string: "\(IMAGE_URL)\(movie.poster)"), placeholderImage: UIImage(named: "placeholder.png"), options: [.continueInBackground, .progressiveDownload], completed: nil)
        
    }
    
    
}

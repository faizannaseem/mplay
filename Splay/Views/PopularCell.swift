//
//  PopularCell.swift
//  Splay
//
//  Created by Faizan on 12/22/17.
//  Copyright © 2017 Faizan. All rights reserved.
//

import UIKit

class PopularCell: UITableViewCell {
    
    
    @IBOutlet weak var voteLbl: UILabel!
    @IBOutlet weak var movieGenre: UILabel!
    @IBOutlet weak var movieName: UILabel!
    @IBOutlet weak var movieImg: UIImageView!
    @IBOutlet weak var overview: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        movieImg.layer.cornerRadius = 3.0
        
    }

    func configureCell(movie: Movie)
    {
        voteLbl.text = "\(movie.vote)"
        movieGenre.text = movie.genre
        movieName.text = movie.title
        overview.text = movie.overview
        movieImg.sd_setImage(with: URL(string: "\(IMAGE_URL)\(movie.poster)"), placeholderImage: UIImage(named: "placeholder.png"), options: [.continueInBackground, .progressiveDownload], completed: nil)
        
    }
    
    
}

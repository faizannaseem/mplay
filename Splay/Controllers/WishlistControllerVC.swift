//
//  WishlistControllerVC.swift
//  Splay
//
//  Created by Faizan on 12/28/17.
//  Copyright © 2017 Faizan. All rights reserved.
//

import UIKit
import RealmSwift

class WishlistControllerVC: UIPageViewController, UIPageViewControllerDataSource {


    
    override func viewDidLoad() {
        super.viewDidLoad()

        dataSource = self
        DataService.instance.getTopRatedMovies { (sucess, error) in
            if(sucess)
            {
                if let startingViewController = self.contentViewController(at: 0) {
                    self.setViewControllers([startingViewController], direction: .forward, animated: true, completion: nil)
                }
            }
            
        }
        
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        var index = (viewController as! WishlistContentVC).index
        index -= 1
        
        return contentViewController(at: index)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        var index = (viewController as! WishlistContentVC).index
        index += 1
        
        return contentViewController(at: index)
    }
    
    
    func contentViewController(at index: Int) -> WishlistContentVC? {
        if index < 0 || index >= 5 {
            return nil
        }
        
        if let pageContentViewController = storyboard?.instantiateViewController(withIdentifier: "WalkthroughWishlistContentViewController") as? WishlistContentVC {
            
            pageContentViewController.imageFile = DataService.instance.topRated[index].poster
            pageContentViewController.heading = DataService.instance.topRated[index].title
            pageContentViewController.content = DataService.instance.topRated[index].overview
            pageContentViewController.movieID = DataService.instance.topRated[index].id
            pageContentViewController.date = DataService.instance.topRated[index].release_date
            pageContentViewController.index = index
            
            
            
            return pageContentViewController
        }
        
        return nil
        
        
        
    }
    
    func forward(index: Int) {
        if let nextViewController = contentViewController(at: index + 1) {
            setViewControllers([nextViewController], direction: .forward, animated: true, completion: nil)
        }
    }

    

}

//
//  PopularDetailVC.swift
//  Splay
//
//  Created by Faizan on 12/28/17.
//  Copyright © 2017 Faizan. All rights reserved.
//

import UIKit
import ProgressHUD

class PopularDetailVC: UITableViewController {

    var movieID: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Popular Movies"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        
        DataService.instance.popularMovies { (sucess, error) in
            ProgressHUD.show()
            if(sucess)
            {
                self.tableView.reloadData()
                ProgressHUD.dismiss()
            }
            
        }
        
    }

    


    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataService.instance.popular.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PopularCell", for: indexPath) as! PopularCell
        cell.configureCell(movie: DataService.instance.popular[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        movieID = DataService.instance.popular[indexPath.row].id
        performSegue(withIdentifier: "Detail", sender: self)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Detail"
        {
            let destination = segue.destination as! MovieDetailVC
            destination.movieID = movieID
        }
    }

}

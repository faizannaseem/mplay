//
//  MovieDetailVC.swift
//  Splay
//
//  Created by Faizan on 12/24/17.
//  Copyright © 2017 Faizan. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import ProgressHUD
import YouTubePlayer
import RealmSwift
import ParallaxHeader

class MovieDetailVC: UIViewController, Alertable {

    var movieID :Int!
    var filmGenre: String = ""
    var vote:Float = 0
    var movieName : String = ""
    var posterPath: String = ""
    fileprivate var texts = ["Share Movie", "Add To Wishlist"]
    fileprivate var popoverOptions: [PopoverOption] = [
        .type(.down),
        .blackOverlayColor(UIColor(white: 0.0, alpha: 0.6))
    ]
    
    fileprivate var popover: Popover!
    
    weak var headerImageView: UIView?
    @IBOutlet weak var moreBtn: UIBarButtonItem!
    @IBOutlet weak var movieTagline: UILabel!
    @IBOutlet weak var genreLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var descText: UITextView!
    @IBOutlet weak var movieImg: UIImageView!
    @IBOutlet weak var CastcollectionView: UICollectionView!
    @IBOutlet weak var RecommendcollectionView: UICollectionView!
    @IBOutlet weak var videoView: YouTubePlayerView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var runTimeLbl: UILabel!
    @IBOutlet weak var voteLbl: UILabel!
    @IBOutlet var ViewConstraintHeight : NSLayoutConstraint!
    @IBOutlet weak var mainscrollView: UIScrollView!
    @IBOutlet weak var votesStackView: UIStackView!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        moreBtn.isEnabled = false
        if movieID != nil
        {
            movieDetail(movieID: movieID)
        }
        ProgressHUD.show()
        
        DataService.instance.RecommendedMovies(movieId: movieID) { (sucess, error) in
            
            if(sucess)
            {
                DispatchQueue.main.async {
                    self.RecommendcollectionView.reloadData()
                }
                self.moreBtn.isEnabled = true
                ProgressHUD.dismiss()
            }
            else
            {
                ProgressHUD.dismiss()
            }
        }
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        
        votesStackView.isHidden = true
        movieTagline.text = ""
        genreLbl.text = ""
        dateLbl.text = ""
        descText.text = ""
        runTimeLbl.text = ""
        movieImg.layer.cornerRadius = 3.0
        
    }
    
    func animateContraint()
    {
        ViewConstraintHeight.constant = self.CastcollectionView.frame.origin.y + 200
        UIView.animate(withDuration: 0.1) {
            self.view.layoutIfNeeded()
        }
        
    }
   
    
    private func setupParallaxHeader(url: String) {
        let imageView = UIImageView()
        imageView.sd_setImage(with: URL(string: "\(IMAGE_URL)\(url)"), placeholderImage: UIImage(named: "placeholder.png"), options: [.continueInBackground, .progressiveDownload], completed: nil)
        imageView.contentMode = .scaleAspectFill
        
        headerImageView = imageView
        
        scrollView.parallaxHeader.view = imageView
        scrollView.parallaxHeader.height = 200
        scrollView.parallaxHeader.minimumHeight = 0
        scrollView.parallaxHeader.mode = .topFill
        scrollView.parallaxHeader.parallaxHeaderDidScrollHandler = { parallaxHeader in
        }
    }
    
    

    func movieDetail(movieID: Int)
    {
       Alamofire.request("\(MOVIE_URL)\(movieID)?api_key=\(API_KEY)").responseJSON { (response) in
        
            if response.result.error == nil
            {
                guard let data = response.data else {return}
                let json = try! JSON(data: data)
                
                let backdrop_path = json["backdrop_path"].stringValue
                
                self.setupParallaxHeader(url: backdrop_path)
                
                let poster_path = json["poster_path"].stringValue
                self.movieImg.sd_setImage(with: URL(string: "\(IMAGE_URL)\(poster_path)"), placeholderImage: UIImage(named: "placeholder.png"), options: [.continueInBackground, .progressiveDownload], completed: nil)
                
                let original_title = json["original_title"].stringValue
                self.title = original_title
                self.movieName = original_title
                
                let overview = json["overview"].stringValue
                self.descText.text = overview
                
                self.votesStackView.isHidden = false
                let vote = json["vote_average"].floatValue
                self.voteLbl.text = "\(vote)"
                self.vote  = vote
                
                let runtime = json["runtime"].intValue
                self.runTimeLbl.text  = "\(runtime) mins"
                
                self.animateContraint()
                
                let poster = json["poster_path"].stringValue
                self.posterPath = poster
                
                let tagline = json["tagline"].stringValue
                if(tagline == "")
                {
                    self.movieTagline.text = original_title
                }
                else
                {
                    self.movieTagline.text = tagline
                }
            
                if let genreIds = json["genres"].array
                {
                    for genreId in genreIds
                    {
                        
                        let name = genreId["name"].stringValue
                        self.filmGenre.append("\(name), ")
                        
                    }
                    self.genreLbl.text = "Genre: \(self.filmGenre)"
                }
                
                let release_date = json["release_date"].stringValue
                self.dateFormatter(date: "\(release_date)")
                
              
            }
            else
            {
                print(response.result.error!.localizedDescription)
            }
        }
        
        Alamofire.request("\(MOVIE_URL)\(movieID)/videos?api_key=\(API_KEY)").responseJSON { (response) in
            if response.result.error == nil
            {
                guard let data = response.data else {return}
                let json = try! JSON(data: data)
                if let results = json["results"].array
                {
                    for result in results
                    {
                        let video_key = result["key"].stringValue
                        self.getVideo(video: video_key)
                        break
                    }
                }
                
                
            }
            else
            {
                print(response.result.error!.localizedDescription)
                self.showAlert(_message: response.result.error!.localizedDescription)
            }
        }
        
        DataService.instance.castMovies(movieID: movieID) { (sucess, error) in
            if sucess
            {
                self.CastcollectionView.reloadData()
            }
        }
        
    }
    
    
    func getVideo(video: String)
    {
        videoView.playerVars = ["playsinline":1 as AnyObject,
                                "fs":0 as AnyObject,
                                "showinfo":0 as AnyObject]
        DispatchQueue.main.async {
            self.videoView.loadVideoID("\(video)")
        }
        
    }
    
    func dateFormatter(date: String)
    {
        let InputdateFormatter = DateFormatter()
        InputdateFormatter.dateFormat = "YYYY-MM-dd"
        
        let outputFormatter = DateFormatter()
        outputFormatter.dateFormat = "MMM dd, YYYY"
        
        let showDate = InputdateFormatter.date(from: date)
        if showDate != nil
        {
            let resultString = outputFormatter.string(from: showDate!)
            dateLbl.text = "Released Date: \(resultString)"
        }
        else
        {
            dateLbl.text = " "
        }
        
        
    }
    
    
   
 
    
    
    func wishlistPressed()
    {
        let alert = UIAlertController(title: "Add to Wishlist", message: "You want to add movie into your favorites ?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "No", style: .destructive, handler: nil))
        alert.addAction(UIAlertAction(title: "Continue", style: .default, handler: { (action) in
            
            let poster_url = "\(IMAGE_URL)\(self.posterPath)"
            let imageData = try! Data(contentsOf: URL(string: poster_url)!)
            let poster_image: UIImage = UIImage(data: imageData)!
            let lowResImgData: Data = UIImageJPEGRepresentation(poster_image, 0.6)!
            
            let movie = MovieDB()
            movie.date = Date()
            movie.id = self.movieID
            movie.genre = self.filmGenre
            movie.title = self.movieName
            movie.vote = self.vote
            movie.poster = lowResImgData
            movie.taskId = String(self.movieID)
            let realm = try! Realm()
            try! realm.write {
                
                print("Adding data")
                realm.add(movie, update: true)
                self.movieAddedSuccessfully()
            }
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func movieAddedSuccessfully()
    {
        let alert = UIAlertController(title: "Yours Wishlist", message: "Movie added successfully into your wishlist.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    @IBAction func moreTapped(_ sender: Any) {
        
        let startPoint = CGPoint(x: self.view.frame.width - 30, y: 70)
        let tableView = UITableView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 135))
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isScrollEnabled = false
        self.popover = Popover(options: self.popoverOptions)
        self.popover.show(tableView, point: startPoint)
    }
    
    
}



extension MovieDetailVC: UICollectionViewDelegate, UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 100
        {
            return DataService.instance.similar.count
        }
        if collectionView.tag == 99
        {
            return DataService.instance.cast.count
        }
        else
        {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 100
        {
            let cell = RecommendcollectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! RecommendedMovieCell
            cell.configureCell(movie: DataService.instance.similar[indexPath.row])
            return cell
        }
        if collectionView.tag == 99
        {
            let cell = CastcollectionView.dequeueReusableCell(withReuseIdentifier: "CastCell", for: indexPath) as! CastCell
            cell.configureCell(cast: DataService.instance.cast[indexPath.row])
            return cell
        }
        else
        {
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 100
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "DetailVC") as! MovieDetailVC
            vc.movieID = DataService.instance.similar[indexPath.row].id
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}



extension MovieDetailVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.popover.dismiss()
        if indexPath.row == 0
        {
            let activityController = UIActivityViewController(activityItems: ["\(movieTagline.text ?? "") Hey Watch this movie !!!",movieImg.image!], applicationActivities: nil)
            present(activityController, animated: true, completion: nil)
        }
        if indexPath.row == 1
        {
            wishlistPressed()
        }
    }
}

extension MovieDetailVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return texts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
        cell.textLabel?.text = self.texts[(indexPath as NSIndexPath).row]
        cell.imageView?.image = UIImage(named: self.texts[(indexPath as NSIndexPath).row])
        return cell
    }
    
    
}


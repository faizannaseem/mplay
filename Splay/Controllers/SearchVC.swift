//
//  SearchVC.swift
//  Splay
//
//  Created by Faizan on 12/28/17.
//  Copyright © 2017 Faizan. All rights reserved.
//

import UIKit
import ProgressHUD

class SearchVC: UITableViewController, UISearchControllerDelegate, Alertable {
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    var movieID: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        customizeBar()

    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
    }
    
    func customizeBar()
    {
        searchBar.delegate = self
        searchBar.returnKeyType = UIReturnKeyType.search
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = UIColor.white
        
        let textFieldInsideSearchBarLabel = textFieldInsideSearchBar!.value(forKey: "placeholderLabel") as? UILabel
        textFieldInsideSearchBarLabel?.textColor = UIColor.white
        
        let clearButton = textFieldInsideSearchBar?.value(forKey: "clearButton") as! UIButton
        clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(.alwaysTemplate), for: .normal)
        clearButton.tintColor = UIColor.white
        
        let glassIconView = textFieldInsideSearchBar?.leftView as? UIImageView
        
        glassIconView?.image = glassIconView?.image?.withRenderingMode(.alwaysTemplate)
        glassIconView?.tintColor = UIColor.white
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SearchDetail"
        {
            let destination = segue.destination as! MovieDetailVC
            destination.movieID = movieID
        }
    }

}




extension SearchVC
{
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataService.instance.search.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SearchCell
        cell.configureCell(movie: DataService.instance.search[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        movieID = DataService.instance.search[indexPath.row].id
        performSegue(withIdentifier: "SearchDetail", sender: nil)
    }
    
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
}



extension SearchVC: UISearchBarDelegate
{
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        view.endEditing(true)
        ProgressHUD.show()
        let searchText = searchBar.text?.replacingOccurrences(of: " ", with: "%20")
        DataService.instance.searchMovies(query: searchText!) { (sucess, error) in
           
            if sucess{
                ProgressHUD.dismiss()
                self.tableView.reloadData()
            }
            else
            {
                ProgressHUD.dismiss()
                self.showAlert(_message: error!)
            }
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchBar.text == nil || searchBar.text == "" {
            
            searchBar.setShowsCancelButton(false, animated: true)
            view.endEditing(true)
        }
        else
        {
            searchBar.setShowsCancelButton(true, animated: true)
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
    }
       
}










//
//  HomeVC.swift
//  Splay
//
//  Created by Faizan on 11/21/17.
//  Copyright © 2017 Faizan. All rights reserved.
//

import UIKit
import ProgressHUD
import RevealingSplashView
import GoogleMobileAds


class HomeVC: UITableViewController, Alertable {

    @IBOutlet weak var nowplayingCollectionView: UICollectionView!
    @IBOutlet weak var popularTableView: UITableView!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var bannerView: GADBannerView!
    
    var dataSource = PopularTable()
    var movieID: Int = 0
    
    let revealingSplashView = RevealingSplashView(iconImage: UIImage(named: "splash")!, iconInitialSize: CGSize(width: 240.0, height: 128.0), backgroundColor: #colorLiteral(red: 0.09803921569, green: 0.09803921569, blue: 0.09803921569, alpha: 1))
    
    override func viewDidLoad() {
        super.viewDidLoad()

        revealingSplashView.animationType = .heartBeat
        revealingSplashView.heartAttack = true
        
        let window = UIApplication.shared.keyWindow
        window?.addSubview(revealingSplashView)
 
        let request  = GADRequest()
        request.testDevices = ["0b28d9ebfd04b2a330f86603fafb690a"]
        bannerView.adUnitID = ADMOB_APP_UNIT
        bannerView.rootViewController = self
        bannerView.load(request)
        
        revealingSplashView.startAnimation()
        
        
        
        if revealViewController() != nil {
            menuButton.target = revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        
        
        DataService.instance.NowPlaying { (sucess, error) in

            
            if(sucess)
            {
                self.nowplayingCollectionView.reloadData()
            }
            else
            {
                self.showAlert(_message: error!)
            }
        }
        
        DataService.instance.popularMovies { (sucess, error) in
            if(sucess)
            {
                self.popularTableView.reloadData()
            }
            
        
        }
    
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)

        popularTableView.dataSource = dataSource
        popularTableView.delegate = dataSource
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Detail"
        {
            let destination = segue.destination as! MovieDetailVC
            destination.movieID = movieID
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }

}


extension HomeVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return DataService.instance.nowplaying.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = nowplayingCollectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! NowPlayCell
        cell.configureCell(movie: DataService.instance.nowplaying[indexPath.row])
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        movieID = DataService.instance.nowplaying[indexPath.row].id
        performSegue(withIdentifier: "Detail", sender: self)
    }

}

class PopularTable: NSObject, UITableViewDataSource, UITableViewDelegate
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if DataService.instance.popular.count > 3
        {
            return 3
        }
        
        return DataService.instance.popular.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PopularCell", for: indexPath) as! PopularCell
        cell.configureCell(movie: DataService.instance.popular[indexPath.row])
        return cell
    }
    
    
    
}




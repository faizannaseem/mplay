//
//  LeftMenuVC.swift
//  Splay
//
//  Created by Faizan on 08/01/2018.
//  Copyright © 2018 Faizan. All rights reserved.
//

import UIKit

class LeftMenuVC: UIViewController {
    
    var blurEffectView = UIVisualEffectView()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.revealViewController().rearViewRevealWidth = self.view.frame.size.width - 60
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        blurEffectView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.dark))
        blurEffectView.addGestureRecognizer(revealViewController().tapGestureRecognizer())
        blurEffectView.alpha = 0.5
        blurEffectView.frame = self.revealViewController().frontViewController.view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.revealViewController().frontViewController.view.addSubview(blurEffectView)
        
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        blurEffectView.removeFromSuperview()
    }
    
    
    
    
    


}

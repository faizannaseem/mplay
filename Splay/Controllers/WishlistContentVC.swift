//
//  WishlistContentVC.swift
//  Splay
//
//  Created by Faizan on 12/28/17.
//  Copyright © 2017 Faizan. All rights reserved.
//

import UIKit

class WishlistContentVC: UIViewController {
    
    var heading = ""
    var imageFile = ""
    var content = ""
    var movieID = 0
    var date = ""
    var index = 0
    
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet weak var movieImg: UIImageView!
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var timeAdd: UILabel!
    @IBOutlet weak var overViewText: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        movieImg.layer.cornerRadius = 2.0
        dateFormatter(date: date)
        overViewText.text = content
        movieTitle.text = heading
        pageControl.currentPage = index
        movieImg.sd_setImage(with: URL(string: "\(IMAGE_URL)\(imageFile)"), placeholderImage: UIImage(named: "placeholder.png"), options: [.continueInBackground, .progressiveDownload], completed: nil)
        
        
    }

    
    func dateFormatter(date: String)
    {
        let InputdateFormatter = DateFormatter()
        InputdateFormatter.dateFormat = "YYYY-MM-dd"
        
        let outputFormatter = DateFormatter()
        outputFormatter.dateFormat = "MMM dd, YYYY"
        
        let showDate = InputdateFormatter.date(from: date)
        if showDate != nil
        {
            let resultString = outputFormatter.string(from: showDate!)
            timeAdd.text = resultString
        }
        else
        {
            timeAdd.text = " "
        }
        
        
    }
    
    
    

   

}

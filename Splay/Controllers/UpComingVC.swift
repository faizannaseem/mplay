//
//  UpComingVC.swift
//  Splay
//
//  Created by Faizan on 12/23/17.
//  Copyright © 2017 Faizan. All rights reserved.
//

import UIKit
import RealmSwift

class UpComingVC: UIViewController {
    
    @IBOutlet weak var movieName: UILabel!
    @IBOutlet weak var movieImg: UIImageView!
    @IBOutlet weak var overview: UILabel!
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet weak var watchTrailerBtn: UIButton!
    @IBOutlet weak var addWatchListBtn: UIButton!
    
    var index = 0
    var heading = ""
    var imageFile = ""
    var content = ""
    var vote : Float = 0
    var genre = ""
    var movieID = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        watchTrailerBtn.layer.cornerRadius = 25.0
        addWatchListBtn.layer.cornerRadius = 25.0
        addWatchListBtn.layer.borderColor = UIColor.white.cgColor
        addWatchListBtn.layer.borderWidth = 2.0

        overview.text = content
        movieName.text = heading
        pageControl.currentPage = index
        movieImg.sd_setImage(with: URL(string: "\(IMAGE_URL)\(imageFile)"), placeholderImage: UIImage(named: "placeholder.png"), options: [.continueInBackground, .progressiveDownload], completed: nil)
        
    }

    @IBAction func watchTrailerTapped(_ sender: Any) {
        performSegue(withIdentifier: "Detail", sender: self)
    }
    
    @IBAction func addWatchListTapped(_ sender: Any) {
        let poster_url = "\(IMAGE_URL)\(self.imageFile)"
        guard let imageData = try? Data(contentsOf: URL(string: poster_url)!) else {return}
        let poster_image: UIImage = UIImage(data: imageData)!
        let lowResImgData: Data = UIImageJPEGRepresentation(poster_image, 0.6)!
        
        let movie = MovieDB()
        movie.date = Date()
        movie.id = self.movieID
        movie.genre = self.genre
        movie.title = self.heading
        movie.vote = self.vote
        movie.poster = lowResImgData
        movie.taskId = String(self.movieID)
        let realm = try! Realm()
        try! realm.write {
            
            print("Adding data")
            realm.add(movie, update: true)
            self.movieAddedSuccessfully()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Detail"
        {
            let destination = segue.destination as! MovieDetailVC
            destination.movieID = movieID
        }
    }
    
    func movieAddedSuccessfully()
    {
        let alert = UIAlertController(title: "Yours Wishlist", message: "Movie added successfully into your wishlist.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }

}

//
//  WishlistDetailVC.swift
//  Splay
//
//  Created by Faizan on 12/29/17.
//  Copyright © 2017 Faizan. All rights reserved.
//

import UIKit
import RealmSwift

class WishlistDetailVC: UITableViewController {

    var movies : Results<MovieDB>!
    var subscription: NotificationToken?
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if revealViewController() != nil {
            menuButton.target = revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        
        let realm = try! Realm()
        movies = realm.objects(MovieDB.self).sorted(byKeyPath: "date", ascending: false)
        subscription = notificationSubscription(movies)
        
    }

    
    
    
    
    func notificationSubscription(_ tasks: Results<MovieDB>) -> NotificationToken {
        return tasks.observe {[weak self] (changes: RealmCollectionChange<Results<MovieDB>>) in
            self?.updateUI(changes)
        }
    }
    
    
    func updateUI(_ changes: RealmCollectionChange<Results<MovieDB>>) {
        switch changes {
        case .initial(_):
            tableView.reloadData()
        case .update(_, let deletions, let insertions, let modifications):
            
            tableView.beginUpdates()
            tableView.insertRows(at: insertions.map({ IndexPath(row: $0, section: 0) }),
                                 with: .fade)
            tableView.deleteRows(at: deletions.map({ IndexPath(row: $0, section: 0)}),
                                 with: .fade)
            tableView.reloadRows(at: modifications.map({ IndexPath(row: $0, section: 0) }),
                                 with: .fade)
            tableView.endUpdates()
            break
            
            
        case .error(let error):
            print(error)
        }
    }
    
    

}


extension WishlistDetailVC
{
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movies.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! WishlistCell
        cell.configureCell(movie: movies[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCellEditingStyle.delete{
            if let item = movies?[indexPath.row] {
                let realm = try! Realm()
                try! realm.write {
                    realm.delete(item)
                }
            }
        }
    }
    
}









//
//  TopRatedVC.swift
//  Splay
//
//  Created by Faizan on 08/01/2018.
//  Copyright © 2018 Faizan. All rights reserved.
//

import UIKit
import ProgressHUD

class TopRatedVC: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Top Rated Movies"
        
        DataService.instance.getTopRatedMovies { (sucess, error) in
            ProgressHUD.show()
            if(sucess)
            {
                self.tableView.reloadData()
                print(DataService.instance.topRated)
                ProgressHUD.dismiss()
            }
            
        }
        
    }


    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataService.instance.topRated.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TopRatedCell", for: indexPath) as! TopRatedCell
        cell.configureCell(movie: DataService.instance.topRated[indexPath.row])
        return cell
    }

}

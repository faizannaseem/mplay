//
//  UpComingPageVC.swift
//  Splay
//
//  Created by Faizan on 12/23/17.
//  Copyright © 2017 Faizan. All rights reserved.
//

import UIKit

class UpComingPageVC: UIPageViewController, UIPageViewControllerDataSource {

    override func viewDidLoad() {
        super.viewDidLoad()

        dataSource = self

        
        
        DataService.instance.upcomingMovies { (sucess, error) in
            if(sucess)
            {
                if let startingViewController = self.contentViewController(at: 0) {
                    self.setViewControllers([startingViewController], direction: .forward, animated: true, completion: nil)
                }
            }

        }
        
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        var index = (viewController as! UpComingVC).index
        index -= 1
        
        return contentViewController(at: index)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        var index = (viewController as! UpComingVC).index
        index += 1
        
        return contentViewController(at: index)
    }
    
    // MARK: - Helper Methods
    
    func contentViewController(at index: Int) -> UpComingVC? {
        if index < 0 || index >= 5 {
            return nil
        }
        
        if let pageContentViewController = storyboard?.instantiateViewController(withIdentifier: "WalkthroughUpComingContentViewController") as? UpComingVC {
            
            pageContentViewController.imageFile = DataService.instance.upcoming[index].poster
            pageContentViewController.heading = DataService.instance.upcoming[index].title
            pageContentViewController.content = DataService.instance.upcoming[index].overview
            pageContentViewController.movieID = DataService.instance.upcoming[index].id
            pageContentViewController.genre = DataService.instance.upcoming[index].genre
            pageContentViewController.vote = DataService.instance.upcoming[index].vote
            pageContentViewController.index = index
            
            
            
            return pageContentViewController
        }
        
        return nil
        
        
        
    }
    
    func forward(index: Int) {
        if let nextViewController = contentViewController(at: index + 1) {
            setViewControllers([nextViewController], direction: .forward, animated: true, completion: nil)
        }
    }


}
